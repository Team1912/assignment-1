all:
	pdflatex *.tex

clean:
	/bin/rm -f *.aux *~ *.log *.gz 
remake: clean all
